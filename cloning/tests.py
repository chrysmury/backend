from rest_framework.test import APIClient, APITestCase
from rest_framework import status
from django.urls import reverse, resolve
from cloning.views import cloneAP
from authentication.models import User


class TestCloning(APITestCase):
    def setUp(self):
        self.url = reverse('cloning:clone_ap')
        self.view = resolve(self.url)

    def test_view_resolution(self):
        print(self.url)
        self.assertEqual(self.view.func, cloneAP)

    def test_invalid_ap_details(self):
        pass