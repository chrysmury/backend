from rest_framework.exceptions import APIException

class APDataError(APIException):
    status_code = 400
    default_detail = "Invalid access point details."

class APFailedHPDNS(APIException):
    status_code = 417
    default_detail = "Cloning failed. DNS and Hostapd."

class APFailedPortal(APIException):
    status_code = 417
    default_detail = "Failed at portal config."

class TerminationFail(APIException):
    status_code = 417
    default_detail = "Termination failed."

class SnifferFail(APIException):
    status_code = 417
    default_detail = "Failed to restart sniffer."