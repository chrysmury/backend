from django.shortcuts import render
from rest_framework.decorators import api_view
from .createap import CreateAp
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes


from .exceptions import (APDataError,
                        APFailedPortal,
                        APFailedHPDNS,
                        TerminationFail,
                        SnifferFail)


import subprocess
from .constants import *


def run_command_line(cmd):
    ''' run terminal commands '''
    try:
        subprocess.Popen(cmd.split(),stdout=subprocess.PIPE)
    except:
        pass

@api_view(('POST',))
@permission_classes((IsAuthenticated,))
def cloneAP(request, *args, **kwargs):
    print(request.data)
    ap_details = request.data.get('ap_details', {})
    print(ap_details)
    if len(ap_details) == 0:
        raise APDataError
    ssid = ap_details['ssid']
    bssid = ap_details['bssid']
    channel = ap_details['channel']
    security = ap_details['security']
    portal = ap_details['portal']

    if security == 'secure':
        password = ap_details['password']
    else:
        password = ""        

    AccessPoint = CreateAp(ssid, bssid, channel, password, security)
    try:
        # Initialize interfaces
        AccessPoint.initializeInterface()

        # Set Config
        print("[*] Setting up configuration, hostapd initializing...")
        if security =="secure":
            AccessPoint.generateConfigHAP(HOSTAPD_CONF_S)
        elif security =="open":
            AccessPoint.generateConfigHAP(HOSTAPD_CONF_O)

        print("[*] Starting hostapd...")
        AccessPoint.startHostapd(type=security)
        print("[*] Starting dnsmasq...")
        AccessPoint.startDNS()

        CreateAp._instance = AccessPoint._get_instance()

    except Exception as e:
        raise APFailedHPDNS

    # Set IpTables and Fowarding
    print("[*] Setting Traffic Forwarding...")
    if portal == "no":
        try:
            AccessPoint.route_no_portal()
        except:
            print("failing here")
            raise APFailedPortal

    elif portal == "yes":
        try:
            AccessPoint.route_to_portal()
        except Exception as e:
            raise APFailedPortal
    
    return Response({"success": "Cloning successful."}, status=status.HTTP_200_OK)


@api_view(['GET',])
@permission_classes((IsAuthenticated,))
def terminateAP(request, *args, **kwargs):
    try:
        ap = CreateAp._get_instance()            
        ap.stopDNS()
        ap.stopHostapd()
    except:
        raise TerminationFail

    # clean iptables
    try:
        run_command_line("iptables -t nat -F")
        run_command_line("iptables -F")
    except:
        pass
    return Response({"success": "Termination successful."}, status=status.HTTP_200_OK)
 
@api_view(['GET',])
def startSniffer(request, *args, **kwargs):
    print("Starting Sniffer")
    try:
        proc = subprocess.Popen(KILL_SNIFFER,stdout = subprocess.PIPE)
        proc = subprocess.Popen(INIT_SNIFFER_INT, stdout = subprocess.PIPE)
        proc = subprocess.Popen(SNIFFER_CMD, stdout = subprocess.PIPE)
        return Response({"success": "Sniffer restarted."}, status=status.HTTP_200_OK)
    except:
        raise SnifferFail


