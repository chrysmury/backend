from django.urls import path

from .views import (
    cloneAP,
    terminateAP,
    startSniffer,
)

app_name = 'cloning'

urlpatterns = [
    path('clone_ap', cloneAP, name='clone_ap'),
    path('terminate_ap', terminateAP, name='terminate_ap'),
    path('start_sniffer', startSniffer, name='start_sniffer')
]
