SCANNER_CMD = "sudo /bin/bash /home/tom/code/dar_code/bettercap_back/start_scanner_bettercap.sh".split()
SNIFFER_CMD  = "sudo /bin/bash /home/tom/code/dar_code/bettercap_back/start_sniffer_bettercap.sh".split()
INIT_SCANNER_INT = "sudo /bin/bash /home/tom/code/dar_code/bettercap_back/init_scanner_int.sh".split()#["sudo ifconfig wlan1 down"," sudo ifconfig wlan2 down","sudo iwconfig wlan1 mode monitor","sudo iwconfig wlan2 mode managed","sudo ifconfig wlan1 up","sudo ifconfig wlan2 up"]
INIT_SNIFFER_INT = "sudo /bin/bash /home/tom/code/dar_code/bettercap_back/init_sniffer_int.sh".split()
KILL_SCANNER = "sudo /usr/bin/fuser -k 8081/tcp".split()
KILL_SNIFFER = "sudo /usr/bin/fuser -k 8082/tcp".split()

HOSTAPD_CONF_S = '/etc/hostapd/hostapds.conf' # for secure wifi
HOSTAPD_CONF_O = '/etc/hostapd/hostapdo.conf' # for open wifi

DNSMASQ_CONF = '/etc/dnsmasq.conf'


DNSMASQ_LOG = '/var/log/dnsmasq.log'