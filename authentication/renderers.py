import json
from rest_framework.renderers import JSONRenderer
from core.renderers import BaseRenderer

class UserJSONRenderer(BaseRenderer):
    obj_label = 'users'
    def render(self, data, media_type=None, renderer_context=None):
        token = data.get('token', None)

        if token and isinstance(token, bytes):
            data['token'] = token.decode(self.charset)

        return super(BaseRenderer, self).render(data)
        

class AllUserJSONRenderer(BaseRenderer):
    obj_label = 'allusers'
    def render(self, data, media_typea, renderer_context=None):
        return super(BaseRenderer, self).render(data)