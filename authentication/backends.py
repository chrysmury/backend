import jwt

from django.conf import settings

from rest_framework import authentication, exceptions

from .models import User

import base64

class JWTAuthentication(authentication.BaseAuthentication):
    authentication_header_prefix = "Bearer"
    charset = 'utf-8'

    def authenticate(self, request):
        request.user = None
        auth_header = authentication.get_authorization_header(request).split()
        
        if auth_header is None:
            return None # no auth header

        if len(auth_header) != 2:
            return None # this is not sth we know
        prefix, token = [*auth_header]

        prefix = prefix.decode(self.charset)
        token = token.decode(self.charset)

        try:
            token = eval(token)
        except:
            pass

        if prefix.lower() != self.authentication_header_prefix.lower():
            return None # we need the work bearer there or sth known
        return self._authenticate_credentials(request, token)
    
    def _authenticate_credentials(self, request, token):
        try:
            payload = jwt.decode(token, key=settings.SECRET_KEY)
        except Exception as e:
            msg = "could not decode the token"
            raise exceptions.AuthenticationFailed(msg)
        
        try:
            user = User.objects.get(pk=payload['id'])
        except:
            msg = "No user matches this token"
            raise exceptions.AuthenticationFailed(msg)

        if not user.is_active:
            msg = "The user has been deactivated"
            raise exceptions.AuthenticationFailed(msg)

        return (user, token)

        
