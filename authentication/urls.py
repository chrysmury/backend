from django.urls import path
from authentication.views import (
    RegistrationAPIView, 
    LoginAPIView,
    UserRetrieveUpdateAPIView,
    check_superuser,
    AllUserRetriveUpdate,
    SuspendUserUpdateAPIView,
)

app_name ='authentication'

urlpatterns = [
    path('register', RegistrationAPIView.as_view(), name='register'),
    path('login', LoginAPIView.as_view(), name='login'),
    path('user', UserRetrieveUpdateAPIView.as_view(), name='user'),
    path('superuser', check_superuser, name='check_superuser'),
    path('allusers', AllUserRetriveUpdate.as_view(), name='allusers'),
    path('suspend/<str:username>', SuspendUserUpdateAPIView.as_view(), name='suspend')

]

