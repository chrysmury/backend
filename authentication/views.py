from django.shortcuts import render
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import RetrieveUpdateAPIView, RetrieveAPIView, UpdateAPIView
from .serializers import RegistrationSerializer, LoginSerializer, UserSerializer
from .renderers import UserJSONRenderer, AllUserJSONRenderer
from authentication.models import User

@api_view(['POST'])
@permission_classes((AllowAny,))
def check_superuser(request):
    user = request.data.get('username', None)
    superuser = User.objects.get(username=user).is_superuser
    return Response({'superuser':superuser}, status=status.HTTP_200_OK)


class RegistrationAPIView(APIView):
    permission_classes = (IsAdminUser,)
    serializer_class = RegistrationSerializer
    renderer_classes = (UserJSONRenderer,) # put all data under namespace user

    def post(self, request, *args, **kwargs):
        user = request.data.get('user',{})
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class LoginAPIView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = (UserJSONRenderer,)
    serializer_class = LoginSerializer

    def post(self, request):
        user = request.data.get('user', {})
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    

class UserRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class =  UserSerializer
    renderer_classes = (UserJSONRenderer,)

    def retrieve(self, request, *args, **kwargs):
        serializer = self.serializer_class(request.user)
        return  Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        user_data =  request.data.get('user', None)
        serializer_data = {
            'username': user_data.get('username', request.user.username),
            'email': user_data.get('email', request.user.email),

            'profile':{
                'bio': user_data.get('bio', request.user.profile.bio),
                'image': user_data.get('image', request.user.profile.image)
            }
        }

        serializer = self.serializer_class(
            request.user, 
            data = serializer_data, 
            partial = True
            )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)

        
class AllUserRetriveUpdate(RetrieveAPIView):
    permission_classes = (IsAdminUser,)
    renderer_classes = (AllUserJSONRenderer, )
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        print(request)
        serializer_data = User.objects.filter(is_superuser=False)
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class SuspendUserUpdateAPIView(UpdateAPIView):
    permission_classes = (IsAdminUser,)
    renderer_classes = (AllUserJSONRenderer, )
    serializer_class = UserSerializer

    def update(self, request, username,  *args, **kwargs):
        user = User.objects.get(username=username)
        serializer_data = {
            'is_active':False if user.is_active is True else True
        }

        serializer = self.serializer_class(user, data=serializer_data, partial=True)
        serializer.is_valid()
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

