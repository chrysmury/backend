from rest_framework import serializers
from .models import User
from django.contrib.auth import authenticate

from management.serializers import ProfileSerializer
from .models import User

class RegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        min_length=8,
        max_length=128,
        write_only=True
    )
    token = serializers.CharField(max_length = 255, read_only=True)

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'token']

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=255)
    email = serializers.EmailField(max_length=255, read_only=True)
    token = serializers.CharField(max_length=255, read_only=True)
    password = serializers.CharField(max_length=128, min_length=8, write_only=True)

    # read only fields are those that the user is not required to enter but can only be returned from the db
    # write only is for passwords

    def validate(self, data):
        username = data.get('username', None)
        password = data.get('password',None)
        if username is None:
            raise serializers.ValidationError('Enter a username to Log in')

        if password is None:
            raise serializers.ValidationError('Enter a password to Log in')

        # then authenticate this info
        user = authenticate(username=username, password=password)

        if user is None:
            raise serializers.ValidationError('Username or/and password incorrect')

        if not user.is_active:
            raise serializers.ValidationError('This user has been deactivated')
        return {
            'username': user.username,
            'email': user.email,
            'token': user.token,
            'superuser':user.is_superuser
        }


class UserSerializer(serializers.ModelSerializer):
    # we need to be able to get and update users who already exist
    
    password = serializers.CharField(
        max_length = 128,
        min_length=8,
        write_only=True
    )
    profile = ProfileSerializer(write_only=True)
    bio = serializers.CharField(source='profile.bio', read_only=True)
    image = serializers.CharField(source='profile.image', read_only=True)
    created_on = serializers.DateTimeField()
    is_active = serializers.BooleanField()

    class Meta:
        model = User
        fields = ('email', 'username', 'profile', 'password', 'bio', 'image', 'created_on', 'is_active')

    read_only_fields = ('token',)

    def update(self, instance, validated_data):
        # get values in the validated_data(gotten from the request in views) and update the instance
        password = validated_data.pop('password', None)
        profile = validated_data.pop('profile', {})

        for k,v in validated_data.items():
            setattr(instance, k, v)

        if password:
            instance.set_password(password)

        instance.save()

        if profile:
            for (k, v) in profile.items():
                setattr(instance.profile, k, v)

        instance.profile.save()

        return instance

    # We can define a create method here but we want that to be handled by the UserReg
