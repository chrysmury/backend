# signals are usually defined in the signals module
# signal receivers are connected to the ready() methof of the 
# app configuration class
# if using @receiver deco then import the signal submodule in the ready()

from django.apps import AppConfig

class AuthenticationAppConfig(AppConfig):
    name = 'authentication'
    label = 'authentication'
    verbose_name = 'Authentication'

    def ready(self):
        import authentication.signals

    
# then we define the default_app_config, django looks for this by default

default_app_config = 'authentication.AuthenticationAppConfig'