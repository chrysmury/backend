from django.db import models
from datetime import datetime, timedelta
import jwt
from django.conf import settings
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin
)

# create a manager for our custom User model
class UserManager(BaseUserManager):
    def create_user(self, username, email, password):
        if username is None:
            raise TypeError('Users must have a username')
        
        if email is None:
            raise TypeError('Users must have an email address')
        
        if password is None:
            raise TypeError('Users must have a password')

        user = self.model(username=username, email = self.normalize_email(email))
        user.set_password(password) # you cant set password woth setattr as above
        user.save()

        return user

    def create_superuser(self, username, email, password):
        # create the user and then modify some props
        user = self.create_user(username, email, password)
        user.is_superuser = True
        user.is_staff = True

        user.save()

        return user


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(db_index=True, max_length=255, unique=True)
    email = models.EmailField(db_index=True, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ('email',)

    objects = UserManager()

    def __str__(self):
        return self.username

    def get_full_name(self):
        return self.username

    def _generate_token(self):
        exp = datetime.now() + timedelta(days=60)
        payload = {
            'id': self.pk,
            'exp':int(exp.strftime('%s'))
        }
        KEY = settings.SECRET_KEY
        token = jwt.encode(payload, KEY, algorithm='HS256')
        print(type(token))

        return token.decode('utf-8')

    @property
    def token(self):
        return self._generate_token()