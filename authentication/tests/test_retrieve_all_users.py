from authentication.models import User
from rest_framework.test import APIClient, APITestCase
from django.urls import reverse, resolve
from rest_framework import status


class TestAllUserRestrive(APITestCase):
    def setUp(self):
        self.url = reverse('authentication:allusers')
        self.superuser = User.objects.create_superuser(username='darubini', email='daru@bini.com', password='daru@bini')
        self.user = User.objects.create_user(username='user', email='user@test.com', password='user@test')

        self.client = APIClient()
        
    def test_get_users_status_code(self):
        self.client.force_authenticate(self.superuser)
        resp = self.client.get(self.url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        
    def test_suspend_user(self):

        data = {
           "username":'user' 
        }
        url = reverse('authentication:suspend', kwargs={'username':'user'})
        initial = User.objects.get(username='user')
        self.client.force_authenticate(self.superuser)
        resp = self.client.put(url, data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        user = User.objects.get(username='user')
        self.assertNotEqual(user.is_active, initial.is_active)