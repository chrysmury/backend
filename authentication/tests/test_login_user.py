from rest_framework.test import APITestCase, APIClient
from django.test import  RequestFactory
from authentication.views import LoginAPIView, UserRetrieveUpdateAPIView
from django.urls import resolve, reverse
from rest_framework import status
from authentication.models import User


class TestLogin(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.login_url = reverse('authentication:login')
        self.reg_url = reverse('authentication:register')
        self.user_url = reverse('authentication:user')
        self.login_view = resolve(self.login_url)
        user_data = {
            'user': {
                'username':'user1',
                'email':'user@email.com',
                'password':'user1@test'
            }
        }
        
        user = User.objects.create_user(username='user1', email='user@email.com', password='user1@test')
    def test_login_status_code(self):
        login_data = {
            'user':{
                'username':'user1',
                'password':'user1@test'
            }
        }
        resp = self.client.post(self.login_url, login_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn('token', resp.data)
        self.assertEqual('user@email.com', resp.data['email'])

    def test_failed_login(self):
        login_data = {
            'user':{
                'username':'user',
                'password':'user1@test'
            }
        }
        resp = self.client.post(self.login_url, login_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        
    def test_login_resolves_view(self):
        self.assertEqual(self.login_view.func.view_class, LoginAPIView)

    def test_user_resolves_view(self):
        user_view = resolve(self.user_url)
        self.assertEqual(user_view.func.view_class, UserRetrieveUpdateAPIView)

    def test_authentication_get_user(self):
        login_data = {
            'user':{
                'username':'user1',
                'password':'user1@test'
            }
        }
        resp = self.client.post(self.login_url, login_data, format='json')
        self.client.credentials(HTTP_AUTHORIZATION='Bearer '+resp.data['token'])
        resp = self.client.get(self.user_url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_get_user_no_authentication(self):
        user = self.client.get(self.user_url)
        self.assertNotEqual(user.status_code, status.HTTP_200_OK)

    def test_user_profile_combo(self):
        login_data = {
            'user':{
                'username':'user1',
                'password':'user1@test'
            }
        }
        login_resp = self.client.post(self.login_url, login_data, format='json')
        self.client.credentials(HTTP_AUTHORIZATION='Bearer '+login_resp.data['token'])
        resp = self.client.get(self.user_url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertIn('bio', resp.data)
        

    def test_update_user_profile(self):
        update_data = {
            'user': {
                'username':'user2',
                'email':'user@gmail.com',
                'bio':'user update'
            }
        }
        login_data = {
            'user':{
                'username':'user1',
                'password':'user1@test'
            }
        }
        login_resp = self.client.post(self.login_url, login_data, format='json')
        self.client.credentials(HTTP_AUTHORIZATION='Bearer '+login_resp.data['token'])
        update_resp = self.client.put(self.user_url, update_data, format='json')
        self.assertEqual(update_resp.status_code, status.HTTP_200_OK)
        self.assertEqual(update_resp.data['username'], update_data['user']['username'])
        user = self.client.get(self.user_url)
        self.assertEqual(user.data['bio'], 'user update')


        