from rest_framework.test import APITestCase
from django.test import RequestFactory
from authentication.models import User
from management.models import Profile
from management.views import ProfileRetrieveAPIView
from authentication.views import RegistrationAPIView
from django.urls import resolve, reverse
from rest_framework import status


class TestRegistration(APITestCase):
    def setUp(self):
        self.registration_url =  reverse('authentication:register')
        self.start_user = User.objects.create_superuser('testuser', 'test@email.com', 'test@user')

    def test_successful_registration(self):
        user = User.objects.get(username='testuser')
        self.assertEqual(self.start_user, user)
        self.assertEqual(User.objects.count(), 1)

    def test_token_generation(self):
        user = User.objects.get(username='testuser')
        self.assertEqual(self.start_user.token, user.token)

    def test_successful_registraion_api(self):
        self.client.force_authenticate(self.start_user)
        start_user_data = {
            "user":{
                'username': 'darubini',
                'email': 'darubini@t51.club',
                'password': 'darubini'
            }
        }
        resp = self.client.post(self.registration_url, start_user_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 2)
        self.assertEqual(resp.data['username'], start_user_data['user']['username'])
        self.assertEqual(resp.data['email'], start_user_data['user']['email'])
        self.assertEqual(resp.data['token'], User.objects.latest('id').token)

        # Ensure password is write only
        self.assertNotIn('password', resp.data)

    
    def test_url_resolves_view(self):
        view = resolve(self.registration_url)
        self.assertEqual(view.func.view_class, RegistrationAPIView)


    # Test Duplicate Fields 

    def test_duplicate_username_validation(self):
        self.client.force_authenticate(self.start_user)
        user_data = {
            "user": {
                'username':'testuser',
                'email':'test@gmail.com',
                'password':'test@use'
                }
        }
        resp = self.client.post(self.registration_url, user_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST) 
        self.assertEqual(User.objects.count(), 1)

    def test_duplicate_email_validation(self):
        self.client.force_authenticate(self.start_user)
        user_data = {
            "user": {
                'username':'testuser2',
                'email':'test@email.com',
                'password':'test@use'
                }
        }
        resp = self.client.post(self.registration_url, user_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST) 
        self.assertEqual(User.objects.count(), 1)

    # Test Bad and out of bounds Fields

    def test_bad_email_validation(self):
        self.client.force_authenticate(self.start_user)
        user_data = {
            "user": {
                'username':'user2',
                'email':'test',
                'password':'test@use'
                }
        }
        resp = self.client.post(self.registration_url, user_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST) 
        self.assertEqual(User.objects.count(), 1)

    def test_bad_username_validation(self):
        self.client.force_authenticate(self.start_user)
        user_data = {
            "user": {
                'username':'user2'*100,
                'email':'test@email.com',
                'password':'test@use'
                }
        }
        resp = self.client.post(self.registration_url, user_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
    
    def test_short_password_validation(self):
        self.client.force_authenticate(self.start_user)
        user_data = {
            "user": {
                'username':'user2',
                'email':'test@email.com',
                'password':'test'
                }
        }
        resp = self.client.post(self.registration_url, user_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
    
    def test_long_password_validation(self):
        self.client.force_authenticate(self.start_user)
        user_data = {
            "user": {
                'username':'user2',
                'email':'test@email.com',
                'password':'test'
                }
        }
        resp = self.client.post(self.registration_url, user_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)

    def test_no_username_validation(self):
        self.client.force_authenticate(self.start_user)
        user_data = {
            "user": {
                'username':'',
                'email':'test@email.com',
                'password':'test'
                }
        }
        resp = self.client.post(self.registration_url, user_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)

    def test_no_email_validation(self):
        self.client.force_authenticate(self.start_user)
        user_data = {
            "user": {
                'username':'user2',
                'email':'',
                'password':'test'
                }
        }
        resp = self.client.post(self.registration_url, user_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)

    def test_no_password_validation(self):
        self.client.force_authenticate(self.start_user)
        user_data = {
            "user": {
                'username':'user2',
                'email':'test@email.com',
                'password':''
                }
        }
        resp = self.client.post(self.registration_url, user_data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)

class TestProfile(APITestCase):
    def setUp(self):
        self.reg_url = reverse('authentication:register')
        self.profile_url = reverse('management:profile')

        self.start_user = User.objects.create_superuser('testuser', 'test@email.com', 'test@user')
        self.client.force_authenticate(self.start_user)

        user_data = {
            'user':{
                'username':'no_priv',
                'email':'no_priv@email.com',
                'password':'test@user'
            }
        }
        self.user = self.client.post(self.reg_url, user_data, format='json')
        self.profile =  Profile.objects.last()
        self.profile_resp = self.client.get(self.profile_url)

    def test_profile_creation_registration(self):
        self.assertEqual(User.objects.count(), 2)
        self.assertEqual(Profile.objects.count(), 2)

    def test_user_creation_status_code(self):
        self.assertEqual(self.user.status_code, 201)

    def test_correct_user_created(self):
        self.assertEqual(self.profile.user.username, self.user.data['username'])

    def test_url_resolves_profile_view(self):
        view = resolve(self.profile_url)
        self.assertEqual(view.func.view_class, ProfileRetrieveAPIView)

    def test_get_profile_authenticate_status_code(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.user.data['token'])
        resp = self.client.get(self.profile_url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)


        
