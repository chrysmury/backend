from django.db.models.signals import post_save
from django.dispatch import receiver

from management.models import Profile
from .models import User

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, *args, **kwargs):
    # create a profile everytime a user is created
    if instance and created:
        instance.profile = Profile.objects.create(user=instance)    
