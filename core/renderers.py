import json
from rest_framework.renderers import JSONRenderer

class BaseRenderer(JSONRenderer):
    charset = 'utf-8'
    obj_label = 'object'

    def render(self, data, media_type=None, renderer_context=None):
        # tokens are bytes 
        errors = None
        if isinstance(data, list):
            for item in data:
                errors = item.get('errors', None)
                if errors:
                    break
        elif isinstance(data, dict):
            errors = data.get('errors', None)
        # if we get errors we do not render with user namespace
        if errors:
            return super(BaseRenderer, self).render(data)

        return json.dumps({
            self.obj_label:data
        })
        