from rest_framework.views import exception_handler

# We want errors to have namespace errors
def core_exception_handler(exc, context):
    # handler exceptions that we want, the rest we pass to exception_handler
    response = exception_handler(exc, context)
    custom_exceptions = {
        'ValidationError':_handle_generic_errors,
        'ProfileDoesNotExist':_handle_generic_errors,
        'PermissionDenied':_handle_generic_errors,
        'ClientIntegrityError':_handle_generic_errors,
        'AuthenticationFailed':_handle_generic_errors,
        'RecordExists':_handle_generic_errors,
        'RecordDoesNotExist':_handle_generic_errors,
        'APDataError': _handle_generic_errors,
        'APFailedPortal':_handle_generic_errors,
        'APFailedHPDNS': _handle_generic_errors, 
        'TerminationFail':_handle_generic_errors,
        'SnifferFail':_handle_generic_errors
    } 
    _exception_class = exc.__class__.__name__

    if _exception_class in custom_exceptions:
        return custom_exceptions[_exception_class](exc, context, response)

    #Response contains the format of errors from the default exception handler
    return response

def _handle_generic_errors(exc, context, response):
    # create the namespace
    response.data = {
        'errors':response.data
    }
    return response
