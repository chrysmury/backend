from rest_framework import serializers
from .models import (
    Attack,
    Campaign,
    TargetWifi,
    TargetDevice,
    HookedDevice,
    Phish,
    AllAps,
    FreeWifi,
    DeviceProbes,
    CapturedDevice,
    Traffic,
    FreeWifiDevice,
    FreeWifiTraffic
)

class AttackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attack
        fields = '__all__'

    def create(self, validated_data):
        return Attack.objects.create(**validated_data)


class CampaignViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Campaign
        fields = '__all__'

    def create(self, validated_data):
        return Campaign.objects.create(**validated_data)


class TargetWifiSerializer(serializers.ModelSerializer):
    class Meta:
        model = TargetWifi
        fields = '__all__'

    def create(self, validated_data):
        return TargetWifi.objects.create(**validated_data)


class CapturedDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = CapturedDevice
        fields = '__all__'

    def create(self, validated_data):
        return CapturedDevice.objects.create(**validated_data)


class TargetDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = TargetDevice
        fields = '__all__'

    def create(self, validated_data):
        return TargetDevice.objects.create(**validated_data)

class FreeWifiTargetDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = FreeWifiDevice
        fields = '__all__'

    def create(self, validated_data):
        return FreeWifiDevice.objects.create(**validated_data)


class TrafficSerializer(serializers.ModelSerializer):
    class Meta:
        model = Traffic
        fields = '__all__'

    def create(self, validated_data):
        return Traffic.objects.create(**validated_data)

class FreeWifiTrafficSerializer(serializers.ModelSerializer):
    class Meta:
        model = FreeWifiTraffic
        fields = '__all__'

    def create(self, validated_data):
        return FreeWifiTraffic.objects.create(**validated_data)


class HookedDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = HookedDevice
        fields = '__all__'

    def create(self, validated_data):
        return HookedDevice.objects.create(**validated_data)


class PhishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phish
        fields = '__all__'

    def create(self, validated_data):
        return Phish.objects.create(**validated_data)


class AllApsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AllAps
        fields = '__all__'

    def create(self, validated_data):

        return AllAps.objects.create(**validated_data)


class FreeWifiSerializer(serializers.ModelSerializer):
    class Meta:
        model = FreeWifi
        fields = '__all__'

    def create(self, validated_data):
        return FreeWifi.objects.create(**validated_data)


class DeviceProbesSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceProbes
        fields = '__all__'

    def create(self, validated_data):
        return DeviceProbes.objects.create(**validated_data)
