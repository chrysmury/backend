from core.renderers import BaseRenderer


class AttackViewRenderer(BaseRenderer):
    obj_label = "attack"


class CampaignViewRenderer(BaseRenderer):
    obj_label = "campaigns"


class TargetWifiRenderer(BaseRenderer):
    obj_label = "target_wifi"


class TargetDeviceRenderer(BaseRenderer):
    obj_label = "target_device"


class HookedDeviceRenderer(BaseRenderer):
    obj_label = "hooked_device"


class PhishRenderer(BaseRenderer):
    obj_label = "phish"


class AllApsRenderer(BaseRenderer):
    obj_label = 'allaps'


class FreeWifiRenderer(BaseRenderer):
    obj_label = 'freewifi'


class UserActivitiesRenderer(BaseRenderer):
    obj_label = 'user_activities'


class CampaignTargetsRenderer(BaseRenderer):
    obj_label = "campaign_targets"


class DeviceProbesRenderer(BaseRenderer):
    obj_label = 'device_probes'


class CapturedDevicRenderer(BaseRenderer):
    obj_label = "captured_devices"


class TrafficRenderer(BaseRenderer):
    obj_label = "traffic"


class LocationRenderer(BaseRenderer):
    obj_label = "locations"
