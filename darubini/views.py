from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, UpdateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework import status
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, ValidationError
import logging
from authentication.models import User

from .serializers import (
    AttackSerializer,
    CampaignViewSerializer,
    TargetWifiSerializer,
    TargetDeviceSerializer,
    HookedDeviceSerializer,
    PhishSerializer,
    AllApsSerializer,
    FreeWifiSerializer,
    DeviceProbesSerializer,
    CapturedDeviceSerializer,
    TrafficSerializer,
    FreeWifiTargetDeviceSerializer,
    FreeWifiTrafficSerializer
)
from .renderers import (
    AttackViewRenderer,
    CampaignViewRenderer,
    TargetWifiRenderer,
    TargetDeviceRenderer,
    HookedDeviceRenderer,
    PhishRenderer,
    AllApsRenderer,
    FreeWifiRenderer,
    UserActivitiesRenderer,
    CampaignTargetsRenderer,
    DeviceProbesRenderer,
    CapturedDevicRenderer,
    TrafficRenderer,
    LocationRenderer
)
from .models import (
    Attack,
    Campaign,
    TargetWifi,
    TargetDevice,
    HookedDevice,
    Phish,
    AllAps,
    FreeWifi,
    DeviceProbes,
    CapturedDevice,
    Traffic,
    FreeWifi,
    FreeWifiDevice,
    FreeWifiTraffic
)

logger = logging.getLogger(__name__)


class AttackAPIView(ListCreateAPIView):
    ''' Handle the particulars of the attack used successfully
    '''
    permission_classes = (IsAuthenticated,)
    serializer_class = AttackSerializer
    renderer_classes = (AttackViewRenderer,)

    def get(self, request, *args, **kwargs):
        serializer_data = Attack.objects.all()
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class CampaignAPIView(ListCreateAPIView):
    '''
        Handle campaign queries. We can have many active session, so we do not restrict
    '''
    permission_classes = (IsAuthenticated,)
    renderer_classes = (CampaignViewRenderer,)
    serializer_class = CampaignViewSerializer

    def get(self, request, *args, **kwargs):
        serializer_data = Campaign.objects.filter(champion=request.user)
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        user = request.user
        serializer_data = request.data.get('campaign', {})
        if serializer_data == {}:
            raise ValidationError
        serializer_data['champion'] = user.id
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class EndCampaignAPIView(APIView):
    '''
        sets the active flag to off for campaign
    '''
    # monitor active and inactive sessions
    permission_classes = (IsAuthenticated,)
    renderer_classes = (CampaignViewRenderer,)

    def get(self, request, campaign, *args, **kwargs):
        active_campaigns = Campaign.objects.filter(name=campaign)
        if active_campaigns:
            active_campaigns.update(active=False)
        return Response({}, status=status.HTTP_200_OK)


class AllAPsAPIView(ListCreateAPIView):
    '''
        Handle accesspoints captured on the main scanner and attach them to the campaign
    '''
    permission_classes = (IsAuthenticated, )
    serializer_class = AllApsSerializer
    renderer_clasprprses = (AllApsRenderer,)

    def get(self, request, *args, **kwargs):
        user = request.user
        if user.is_superuser:
            aps = AllAps.objects.all()
        else:
            aps = AllAps.objects.filter(campaign__champion=user)
        serializer = self.serializer_class(aps, many=True)
        for ap in serializer.data:
            ap['location'] = Campaign.objects.get(id=ap['campaign']).location
            ap['campaign'] = Campaign.objects.get(id=ap['campaign']).name
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        ap = request.data.get('ap', {})
        campaign_name = ap.get('campaign')
        campaign = Campaign.objects.get(name__iexact=campaign_name)

        # replace the name with id (hack)
        serializer_data = request.data.get('ap', {})
        serializer_data['campaign'] = campaign.id
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class DeviceProbesAPIView(ListCreateAPIView):
    ''' 
        Handle captured probes from the scanner
    '''
    permission_classes = (IsAuthenticated, )
    serializer_class = DeviceProbesSerializer
    renderer_classes = (DeviceProbesRenderer,)

    def get(self, request, campaign, *args, **kwargs):
        # return all probes for all campaigns if superuser, and return one campaign probes for other users
        # if user.is_superuser:
        device_probes = DeviceProbes.objects.all()
        # else:
        #     campaign_id = Campaign.objects.get(name_iexact=campaign).id
        #     device_probes = DeviceProbes.objects.filter(campaign_exact=campaign_id)
        serializer = self.serializer_class(device_probes, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, campaign, *args, **kwargs):
        campaign = Campaign.objects.get(name__exact=campaign)
        probe = request.data.get("device_probes", {})
        probe['vendor'] = probe['vendor'] or 'Unknown'
        probe['campaign'] = campaign.id
        serializer = self.serializer_class(data=probe)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class FreeWifiAPIView(ListCreateAPIView):
    '''
        Store the details of the free wifi name and location
    '''
    permission_classes = (IsAuthenticated,)
    serializer_class = FreeWifiSerializer
    renderer_classes = (FreeWifiRenderer, )

    def get(self, request, campaign, *args, **kwargs):
        if campaign == "all":
            serializer_data = FreeWifi.objects.all()
        else:
            serializer_data = FreeWifi.objects.filter(campaign__name=campaign)
    
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, campaign, *args, **kwargs):
        serializer_data = request.data.get('freewifi')
        camp_id = Campaign.objects.get(name__exact=campaign).id
        serializer_data['campaign'] = camp_id
        logger.debug(serializer_data)
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class TargetWifiAPIView(ListCreateAPIView):
    '''
        Store the selected accesspoint.
    '''
    permission_classes = (IsAuthenticated,)
    serializer_class = TargetWifiSerializer
    renderer_classes = (TargetWifiRenderer,)

    def get(self, request, campaign, *args, **kwargs):
        if campaign == "all":
            queryset = TargetWifi.objects.all()
        else:
            campaign = Campaign.objects.get(name=campaign)
            queryset = TargetWifi.objects.filter(campaign=campaign)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, campaign, *args, **kwargs):
        serializer_data = request.data.get('target_wifi', {})
        campaign = Campaign.objects.get(name__exact=campaign)
        serializer_data['campaign'] = campaign.id
        print(serializer_data)
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class CapturedDevicesAPIView(ListCreateAPIView):
    '''
        Store devices connected to each access point that we can. For analytics
        The api call shall contain the campaign and the target wifi since we can have one wifi name appearing on many campaigns
        We should be able to determine which
    '''
    permission_classes = (IsAuthenticated,)
    serializer_class = CapturedDeviceSerializer
    renderer_classes = (CapturedDevicRenderer,)

    def get(self, request, campaign, wifi, *args, **kwargs):
        #campaign_id = Campaign.objects.get(name__iexact=campaign).id
        #wifi_id = TargetWifi.objects.filter(campaign__exact=campaign_id).get(name=wifi)
        serializer_data = CapturedDevice.objects.filter(
            wifi__name=wifi).filter(campaign__name=campaign)
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, campaign, wifi, *args, **kwargs):
        serializer_data = request.data.get('captured_device', {})
        print(serializer_data)
        campaign_id = Campaign.objects.get(name__exact=campaign).id
        wifi_id = AllAps.objects.filter(
            name__iexact=wifi).get(campaign=campaign_id).id
        serializer_data['campaign'] = campaign_id
        serializer_data['wifi'] = wifi_id
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class FreeWifiTargetDeviceAPIView(ListCreateAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = FreeWifiTargetDeviceSerializer
    renderer_classes = (TargetDeviceRenderer,)

    # We need to send the wifi name in the url to specify the get
    # not needed for post but included to specify the target wifi
    def get(self, request, campaign, wifi, *args, **kwargs):
        campaign_id = Campaign.objects.get(name__iexact=campaign).id
        wifi_id = FreeWifi.objects.filter(
            campaign__exact=campaign_id).get(name=wifi).id
        serializer_data = FreeWifiDevice.objects.filter(wifi__exact=wifi_id)
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, campaign, wifi, *args, **kwargs):
        serializer_data = request.data.get('target_device', {})
        # print(serializer_data)
        # if not serializer_data:
        #     raise ValidationError
        campaign_id = Campaign.objects.get(name__iexact=campaign)
        wifi_id = FreeWifi.objects.filter(
            campaign_id=campaign_id).get(name__iexact=wifi)
        serializer_data['wifi'] = wifi_id.id
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class TargetDeviceAPIView(ListCreateAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = TargetDeviceSerializer
    renderer_classes = (TargetDeviceRenderer,)

    # We need to send the wifi name in the url to specify the get
    # not needed for post but included to specify the target wifi
    def get(self, request, campaign, wifi, *args, **kwargs):
        campaign_id = Campaign.objects.get(name__iexact=campaign).id
        wifi_id = TargetWifi.objects.filter(
            campaign__exact=campaign_id).get(name=wifi).id
        serializer_data = TargetDevice.objects.filter(wifi__exact=wifi_id)
        print(serializer_data)
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, campaign, wifi, *args, **kwargs):
        serializer_data = request.data.get('target_device', {})
        # print(serializer_data)
        # if not serializer_data:
        #     raise ValidationError
        campaign_id = Campaign.objects.get(name__iexact=campaign)
        wifi_id = TargetWifi.objects.filter(
            campaign_id=campaign_id).get(name__iexact=wifi)
        serializer_data['wifi'] = wifi_id.id
        print(serializer_data)
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class TrafficAPIView(ListCreateAPIView):
    '''
    Store and view traffic captured on targeted devices
    Post, contains traffic of ip_Address and url
    '''
    permission_classes = (IsAuthenticated, )
    serializer_class = TrafficSerializer
    renderer_classes = (TrafficRenderer, )

    def get(self, request, campaign, wifi, *args, **kwargs):
        #campaign_id = Campaign.objects.get(name__iexact=campaign)
        #wifi_id = TargetWifi.objects.get(campaign__exact=campaign_id).id
        serializer_data = Traffic.objects.filter(
            wifi__name=wifi).filter(campaign__name=campaign)
        #serializer_data = Traffic.objects.filter(wifi__exact=wifi_id)
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, campaign, wifi, *args, **kwargs):
        serializer_data = request.data.get('traffic', {})
        print(serializer_data)
        campaign_id = Campaign.objects.get(name__exact=campaign).id
        wifi_id = TargetWifi.objects.filter(
            name__iexact=wifi).get(campaign=campaign_id).id
        serializer_data['campaign'] = campaign_id
        serializer_data['wifi'] = wifi_id
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class FreeWifiTrafficAPIView(ListCreateAPIView):
    '''
    Store and view traffic captured on targeted devices
    Post, contains traffic of ip_Address and url
    '''
    permission_classes = (IsAuthenticated, )
    serializer_class = FreeWifiTrafficSerializer
    renderer_classes = (TrafficRenderer, )

    def get(self, request, campaign, wifi, *args, **kwargs):
        #campaign_id = Campaign.objects.get(name__iexact=campaign)
        #wifi_id = TargetWifi.objects.get(campaign__exact=campaign_id).id
        serializer_data = FreeWifiTraffic.objects.filter(
            wifi__name=wifi).filter(campaign__name=campaign)
        #serializer_data = Traffic.objects.filter(wifi__exact=wifi_id)
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, campaign, wifi, *args, **kwargs):
        serializer_data = request.data.get('traffic', {})
        print(serializer_data)
        campaign_id = Campaign.objects.get(name__exact=campaign).id
        wifi_id = FreeWifi.objects.filter(
            name__iexact=wifi).get(campaign=campaign_id).id
        serializer_data['campaign'] = campaign_id
        serializer_data['wifi'] = wifi_id
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class HookedDeviceAPIView(ListCreateAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = HookedDeviceSerializer
    renderer_classes = (HookedDeviceRenderer, )

    def get(self, request, *args, **kwargs):
        # only get devices online
        serializer_data = HookedDevice.objects.all()
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer_data = request.data.get('hooked_device', {})
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class PhishAPIView(ListCreateAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = PhishSerializer
    renderer_classes = (PhishRenderer,)

    def get(self, request, *args, **kwargs):
        # only get devices online
        serializer_data = Phish.objects.all()
        serializer = self.serializer_class(serializer_data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer_data = request.data.get('phish', {})
        serializer = self.serializer_class(data=serializer_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class AllCampaignsAPIView(ListAPIView):
    permission_classes = (IsAdminUser,)
    renderer_classes = (CampaignViewRenderer,)
    serializer_class = CampaignViewSerializer

    def get(self, request, *args, **kwargs):
        campaigns = Campaign.objects.all().select_related('champion')
        serializer = self.serializer_class(campaigns, many=True)
        for camp in serializer.data:
            id = camp['champion']
            camp['champion'] = User.objects.get(id=id).username
        return Response(serializer.data, status=status.HTTP_200_OK)


class CampaignsAPSAPIView(ListAPIView):
    permission_classes = (IsAdminUser,)
    renderer_classes = (CampaignViewRenderer,)
    serializer_class = CampaignViewSerializer

    def get(self, request, campaign, *args, **kwargs):
        campaigns = Campaign.objects.get(name__iexact=campaign)
        aps = campaigns.select_related("aps")
        serializer = self.serializer_class(aps, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserActivitiesAPIView(ListAPIView):
    permission_classes = (IsAdminUser, )
    renderer_classes = (UserActivitiesRenderer, )

    def get(self, request, *args, **kwargs):
        all_users = User.objects.all()
        user_campaigns = []
        for user in all_users:
            ap_capt = 0
            targets = 0
            user_data = {}
            campaigns = Campaign.objects.filter(champion__exact=user)
            for campaign in campaigns:
                aps = AllAps.objects.filter(campaign__exact=campaign.id)
                ap_capt += aps.count()
                for ap in aps:
                    targs = TargetDevice.objects.filter(
                        wifi__exact=ap.id).count()
                    targets += targs

            user_data['user'] = user.username
            user_data['campaigns'] = campaigns.count()
            user_data['aps'] = ap_capt
            user_data['targets'] = targets
            user_campaigns.append(user_data)

        return Response(user_campaigns, status=status.HTTP_200_OK)


class CampaignTargetsAPIView(APIView):
    '''
    We need to get the success rate of the campaigns by the number of targets
    '''
    renderer_classes = (CampaignTargetsRenderer,)
    permission_classes = (IsAdminUser,)

    def get(self, request, *args, **kwargs):
        campaigns = Campaign.objects.all()
        campaign_targets = {}
        for campaign in campaigns:
            targets_count = 0
            aps = TargetWifi.objects.filter(campaign__exact=campaign.id)
            for ap in aps:
                targets = TargetDevice.objects.filter(wifi__exact=ap.id)
                targets_count += targets.count()
            campaign_targets[f'{campaign.name} ({campaign.location})'] = targets_count

        return Response(campaign_targets, status=status.HTTP_200_OK)


class LocationFilter(ListAPIView):
    ''' filter devices based on the mac address sent
    get campaign from the devices table and capture locations from that table
    '''
    renderer_classes = (LocationRenderer,)
    permission_classes = (IsAdminUser, )

    def get(self, request, mac, *args, **kwargs):
        #logger.debug(f"previous location requested for {mac}")
        devices = DeviceProbes.objects.filter(mac__iexact=mac)
        targets = TargetDevice.objects.filter(mac__iexact=mac)
        loc_in_target = []
        for target in targets:
            
            loc_in_target.append(location)

        locations = []
        for device in devices:
            location = Campaign.objects.get(name=device.campaign).location
            locations.append(location)
        return Response({"loc":locations, "loc_in_target":loc_in_target}, status=status.HTTP_200_OK)
