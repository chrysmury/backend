from django.test import TestCase

from darubini.models import (
    Campaign,
    AllAps,
    TargetWifi,
    TargetDevice,
    HookedDevice,
    Attack,
    Phish)
from authentication.models import User

class TestModels(TestCase):
    def setUp(self):
        user = User.objects.create_user(username='user1',email="user@email.com", password="user@test")
        campaign = Campaign.objects.create(name="test", champion=user, location="here")
        wifi = TargetWifi.objects.create(
                campaign = campaign,
                name = "wifi",
                mac = "aa:aa:aa:aa:aa:aa",
                vendor = "vendor wifi",
                encryption = "wpa",
                clients = 2
            )
        target = TargetDevice.objects.create(
            name="samsung",
            mac="bb:bb:bb:bb:bb:bb",
            ipaddress = "10.0.0.100",
            wifi = wifi
        )

        hooked_devices = HookedDevice.objects.create(
            ip_address = '192.178.10.10',
            os = "and",
            version='5',
            browser_type="FF",
            session = "garbage"
        )

        allaps = AllAps.objects.create(
            location='loca',
            campaign=campaign,
            name='wifi',
            mac = 'aa:aa:aa:aa:aa:aa',
            vendor = 'samsung',
            encryption='Open',
            clients=2
        )
        attack = Attack.objects.create(name='captive')
        phish = Phish.objects.create(device= hooked_devices,attack_type=attack)

    def test_campaign_creation(self):
        self.assertEqual(Campaign.objects.count(), 1)
    
    def test_targetwifi_creation(self):
        self.assertEqual(TargetWifi.objects.count(), 1)

    def test_targetdevice_creation(self):
        self.assertEqual(TargetDevice.objects.count(), 1)

    def test_hookeddevice_creation(self):
        self.assertEqual(HookedDevice.objects.count(), 1)
    
    def test_attack_creation(self):
        self.assertEqual(Attack.objects.count(), 1)

    def test_phish_creation(self):
        self.assertEqual(Phish.objects.count(), 1) 

    def test_allapps_creation(self):
        self.assertEqual(AllAps.objects.count(), 1)

