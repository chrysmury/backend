""" from django.urls import resolve, reverse
from darubini.models import Phish, Attack
from darubini.views import PhishAPIView
from rest_framework.test import APIClient, APITestCase
from rest_framework import status

class Setup(APITestCase):
    self.url = reverse('darubini:phish')
    self.view = resolve(self.url)
    attack = Attack.objects.create(name="captive")
    phish_data = {
        device:"samsung",
        attack_type:attack
    } """