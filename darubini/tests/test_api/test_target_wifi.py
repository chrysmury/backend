from darubini.models import Campaign
from authentication.models import User
from darubini.views import TargetWifiAPIView
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.urls import reverse, resolve

'''
What is being tested:
- Authentication
- Authorization
- succesful or failed creation
- Duplication of values
- Errors raised
'''

class TestTargetWifi(APITestCase):
    def setUp(self):
        self.target_wifi_url = reverse('darubini:target_wifi')
        self.view = resolve(self.target_wifi_url)
        user = User.objects.create_user(username="daru", email="user@test.com", password="user@test")
        campaign = Campaign.objects.create(champion=user, name='test1', location='stata')
        self.client = APIClient()
        wifi = {
            'target_wifi':{
                'campaign':'test1',
                'name':'wifi1',
                'mac':'00:00:00:00:00:00',
                'vendor':'huawei',
                'encryption':'wpa2',
                'clients':2,
            }
        }
        self.post_resp_no_auth = self.client.post(self.target_wifi_url, wifi, format='json')
        self.get_resp_no_auth = self.client.get(self.target_wifi_url)

        self.client.force_authenticate(user)
        self.post_resp = self.client.post(self.target_wifi_url, wifi, format='json')
        self.get_resp = self.client.get(self.target_wifi_url)

        wifi2 = {
            'target_wifi':{
                'campaign':'test1',
                'name':'wifi2',
                'mac':'00:00:00:00:00:00',
                'vendor':'huawei',
                'encryption':'wpa2',
                'clients':2,
            }
        }
        self.post_resp_dup = self.client.post(self.target_wifi_url, wifi, format='json')
        self.post_resp2 = self.client.post(self.target_wifi_url, wifi2, format='json')

        

    def test_post_no_auth_status_code(self):
        self.assertEqual(self.post_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_auth_status_code(self):
        self.assertEqual(self.post_resp.status_code, status.HTTP_201_CREATED)

    def test_get_no_auth_status_code(self):
        self.assertEqual(self.get_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_auth_status_code(self):
        self.assertEqual(self.get_resp.status_code, status.HTTP_200_OK)

    def test_resolve_view(self):
        self.assertEqual(self.view.func.view_class, TargetWifiAPIView)

    def test_active_for_associated_campaign(self):
        camp = Campaign.objects.get(active=True)
        self.assertEqual(camp.name, 'test1')
        self.assertTrue(camp.active)

    #def test_duplicate_target_wifi_raises(self):
    #    self.assertEqual(self.post_resp_dup.status_code, status.HTTP_400_BAD_REQUEST)
