from darubini.models import Campaign, TargetWifi, TargetDevice
from authentication.models import User
from darubini.views import HookedDeviceAPIView
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.urls import reverse, resolve

'''
What is being tested:
- Authentication
- Authorization
- succesful or failed creation
- Duplication of values
- Errors raised
'''

class TestHookedDevice(APITestCase):
    def setUp(self):
        self.hooked_device_url = reverse('darubini:hooked_device')
        self.view = resolve(self.hooked_device_url)
        user = User.objects.create_user(username="daru", email="user@test.com", password="user@test")
        self.client = APIClient()
        device = {
            'hooked_device':{
                'ip_address':'172.16.2.3',
                'os':'4',
                'version':'S',
                'browser_type':'FF',
                'session':"session"
            }
        }
        self.post_resp_no_auth = self.client.post(self.hooked_device_url, device, format='json')
        self.get_resp_no_auth = self.client.get(self.hooked_device_url)

        self.client.force_authenticate(user)
        self.post_resp = self.client.post(self.hooked_device_url, device, format='json')
        self.get_resp = self.client.get(self.hooked_device_url)



    def test_post_no_auth_status_code(self):
        self.assertEqual(self.post_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_auth_status_code(self):
        self.assertEqual(self.post_resp.status_code, status.HTTP_201_CREATED)

    def test_get_no_auth_status_code(self):
        self.assertEqual(self.get_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_auth_status_code(self):
        self.assertEqual(self.get_resp.status_code, status.HTTP_200_OK)

    def test_resolve_view(self):
        self.assertEqual(self.view.func.view_class, HookedDeviceAPIView)
