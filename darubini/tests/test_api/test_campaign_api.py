from darubini.models import Campaign
from authentication.models import User
from darubini.views import CampaignAPIView
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.urls import reverse, resolve

'''
What is being tested:
- Authentication
- Authorization
- succesful or failed creation
- Duplication of values
- Only one active Campaign at a time
- Errors raised
'''

class TestCampaign(APITestCase):
    def setUp(self):
        self.url = reverse('darubini:campaigns')
        self.end_url = reverse('darubini:end_campaign', kwargs={'campaign':'static1'})
        self.view = resolve(self.url)
        user = User.objects.create_user(username="daru", email="user@test.com", password="user@test")
        campaign = Campaign.objects.create(champion=user, name='static1', location='stata')
        self.client = APIClient()
        campaign_data = {
            'campaign':{
                'name':'ops1',
                'location':'loca',
                'description':'what we like doing'
            }
        }
        self.post_resp_no_auth = self.client.post(self.url, campaign_data, format='json')
        self.get_resp_no_auth = self.client.get(self.url)

        self.client.force_authenticate(user)
        self.post_resp = self.client.post(self.url, campaign_data, format='json')
        self.get_resp = self.client.get(self.url)

        
        
        campaign_data2 = {
            'campaign':{
                'name':'ops2',
                'location':'loca',
                'description':'what we like doing'
            }
        }
        self.post_resp_dup = self.client.post(self.url, campaign_data, format='json')
        self.post_resp2 = self.client.post(self.url, campaign_data2, format='json')

        

    def test_post_no_auth_status_code(self):
        self.assertEqual(self.post_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_auth_status_code(self):
        self.assertEqual(self.post_resp.status_code, status.HTTP_201_CREATED)

    def test_get_no_auth_status_code(self):
        self.assertEqual(self.get_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_auth_status_code(self):
        self.assertEqual(self.get_resp.status_code, status.HTTP_200_OK)

    def test_resolve_view(self):
        self.assertEqual(self.view.func.view_class, CampaignAPIView)

    def test_end_campaign_status_code(self):
        self.end_campaign = self.client.get(self.end_url)
        self.assertEqual(self.end_campaign.status_code, status.HTTP_200_OK)
        active_campaigns = Campaign.objects.filter(active=True)
        self.assertEqual(len(active_campaigns), 2)

    def test_active_status_of_campaigns(self):
        self.assertTrue(self.post_resp.data['active'])

    def test_duplicate_campaigns_raises(self):
        self.assertEqual(self.post_resp_dup.status_code, status.HTTP_400_BAD_REQUEST)

    def test_all_aps(self):
        url = reverse('darubini:all_campaigns')
        resp = self.client.get(url)
        # only superuser are allowed
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

        superuser = User.objects.create_superuser(username='darubini', password='daru@bini', email='daru@bini.com')
        self.client.force_authenticate(superuser)
        resp =self.client.get(url)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data), 3)