from darubini.models import HookedDevice, Attack, Phish
from authentication.models import User
from darubini.views import PhishAPIView
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.urls import reverse, resolve

'''
What is being tested:
- Authentication
- Authorization
- succesful or failed creation
- Duplication of values
- Errors raised
'''

class TestPhish(APITestCase):
    def setUp(self):
        self.phish_url = reverse('darubini:phish')
        self.view = resolve(self.phish_url)
        user = User.objects.create_user(username="daru", email="user@test.com", password="user@test")
        attack = Attack.objects.create(name='portal')
        device = HookedDevice.objects.create(ip_address='172.16.20.10',os='5', version='vers', browser_type='FF', session='session')
        self.client = APIClient()
        phish = {
            'phish':{
                'device':device.id,
                'attack_type':attack.id
            }
        }
        self.client.force_authenticate(user)
        self.post_resp = self.client.post(self.phish_url, phish, format='json')
        self.get_resp = self.client.get(self.phish_url)

        phish2 = {
            'phish':{
                'device':device.id,
                'attack_type':attack.id
            }
        }
        self.post_resp_dup = self.client.post(self.phish_url, phish, format='json')
        self.post_resp2 = self.client.post(self.phish_url, phish2, format='json')

     
    def test_post_auth_status_code(self):
        self.assertEqual(self.post_resp.status_code, status.HTTP_201_CREATED)

    def test_get_auth_status_code(self):
        self.assertEqual(self.get_resp.status_code, status.HTTP_200_OK)

    def test_resolve_view(self):
        self.assertEqual(self.view.func.view_class, PhishAPIView)