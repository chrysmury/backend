from darubini.models import Campaign, AllAps
from authentication.models import User
from darubini.views import AllAPsAPIView
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.urls import reverse, resolve

'''
What is being tested:
- Authentication
- Authorization
- succesful or failed creation
- Duplication of values
- Only one active Campaign at a time
- Errors raised
'''

class TestAllAPs(APITestCase):
    def setUp(self):
        self.url = reverse('darubini:allaps')
        self.view = resolve(self.url)
        user = User.objects.create_user(username="daru", email="user@test.com", password="user@test")
        user2 = User.objects.create_user(username="daru2", email="user2@test.com", password="user@test")
        super_user = User.objects.create_superuser(username="daru_super", email="user_super@test.com", password="user@test")

        campaign = Campaign.objects.create(champion=user, name='static1', location='stata')
        self.client = APIClient()
        data = {'ap':{
              'name':'wifi1',
              'mac':'aa:bb:cc:dd:ee:ff',
              'vendor':'vendor',
              'encryption':'wpa2',
              'clients': 2,
              'campaign':campaign.name,
                }
          }

        self.post_resp_no_auth = self.client.post(self.url, data, format='json')
        self.get_resp_no_auth = self.client.get(self.url)

        self.client.force_authenticate(user)
        self.post_resp = self.client.post(self.url, data, format='json')
        self.get_resp = self.client.get(self.url)
        # another regular user -  Should return no data
        self.client.force_authenticate(user2)
        self.get_user2 = self.client.get(self.url)

        #superuser - should return
        self.client.force_authenticate(super_user)
        self.get_superuser = self.client.get(self.url)


        
        
        data2 = {
            'campaign':{
                'name':'ops2',
                'location':'loca',
                'description':'what we like doing'
            }
        }
        self.post_resp_dup = self.client.post(self.url, data, format='json')
        #self.post_resp2 = self.client.post(self.url, data2, format='json')

        

    def test_post_no_auth_status_code(self):
        self.assertEqual(self.post_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_auth_status_code(self):
        self.assertEqual(self.post_resp.status_code, status.HTTP_201_CREATED)

    def test_get_no_auth_status_code(self):
        self.assertEqual(self.get_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_auth_status_code(self):
        self.assertEqual(self.get_resp.status_code, status.HTTP_200_OK)

    def test_get_user(self):
        self.assertGreater(len(self.get_resp.data), 0)
    
    def test_unauthorized_user(self):
        self.assertEqual(len(self.get_user2.data), 0)

    def test_superuser_get(self):
        self.assertGreater(len(self.get_superuser.data), 0)

    def test_resolve_view(self):
        self.assertEqual(self.view.func.view_class, AllAPsAPIView)
