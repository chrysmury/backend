from darubini.models import Campaign, TargetWifi
from authentication.models import User
from darubini.views import TargetDeviceAPIView
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from django.urls import reverse, resolve

'''
What is being tested:
- Authentication
- Authorization
- succesful or failed creation
- Duplication of values
- Errors raised
'''

class TestTargetDevice(APITestCase):
    def setUp(self):
        self.target_device_url = reverse('darubini:target_device', kwargs={'wifi':'wifi1'})
        self.view = resolve(self.target_device_url)
        user = User.objects.create_user(username="daru", email="user@test.com", password="user@test")
        campaign = Campaign.objects.create(champion=user, name='test1', location='stata')
        target_wifi  = TargetWifi.objects.create(name='wifi1',mac='00:00:00:00:00:00',vendor='huawei',encryption='wpa2',clients=2, campaign=campaign)
        self.client = APIClient()
        device = {
            'target_device':{
                'name':'samsung',
                'vendor':'samsung still',
                'mac':'aa:aa:aa:aa:aa:aa',
                'ipaddress':'172.16.20.10',
                'wifi':target_wifi.name
            }
        }
        self.post_resp_no_auth = self.client.post(self.target_device_url, device, format='json')
        self.get_resp_no_auth = self.client.get(self.target_device_url)

        self.client.force_authenticate(user)
        self.post_resp = self.client.post(self.target_device_url, device, format='json')
        self.get_resp = self.client.get(self.target_device_url)

        device2 = {
            'target_device':{
                'name':'samsung',
                'mac':'aa:aa:aa:aa:aa:ab',
                'ipaddress':'172.16.20.11',
                'wifi':target_wifi.id
            }
        }
        self.post_resp_dup = self.client.post(self.target_device_url, device, format='json')
        self.post_resp2 = self.client.post(self.target_device_url, device2, format='json')

    def test_post_no_auth_status_code(self):
        self.assertEqual(self.post_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_auth_status_code(self):
        self.assertEqual(self.post_resp.status_code, status.HTTP_201_CREATED)

    def test_get_no_auth_status_code(self):
        self.assertEqual(self.get_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_auth_status_code(self):
        self.assertEqual(self.get_resp.status_code, status.HTTP_200_OK)

    def test_resolve_view(self):
        self.assertEqual(self.view.func.view_class, TargetDeviceAPIView)

    #def test_duplicate_target_devices_raises(self):
    #    self.assertEqual(self.post_resp_dup.status_code, status.HTTP_400_BAD_REQUEST)

