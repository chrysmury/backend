from rest_framework.test import APIClient, APITestCase
from darubini.views import AttackAPIView
from management.exceptions import PermissionDenied
from authentication.models import User
from django.urls import reverse, resolve
from rest_framework import status
from  darubini.models import Attack


class TestAttackAPIView(APITestCase):
    def setUp(self):
        self.url = reverse('darubini:attacks')
        user = User.objects.create_user(username='user', email='user@test.com', password='user@test')
        super_user = User.objects.create_superuser(username='superuser', email='super@test.com', password='super@user')
        
        Attack.objects.create(name='forced update')
        self.client = APIClient()
        self.get_resp_no_auth = self.client.get(self.url)

        attack_data = {
            'name':'captive'
        }
        self.client.force_authenticate(user=user)
        self.post_resp = self.client.post(self.url, attack_data, format='json')
        self.get_resp = self.client.get(self.url)

        self.client.force_authenticate(user=super_user)
        self.post_resp_super_user = self.client.post(self.url, attack_data, format='json')

    def test_status_code_get_no_auth(self):
        self.assertEqual(self.get_resp_no_auth.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_status_code(self):
        self.assertEqual(self.get_resp.status_code, status.HTTP_200_OK)
    
    def test_post_status_code(self):
        self.assertEqual(self.post_resp.status_code, status.HTTP_403_FORBIDDEN)

    def test_post_status_code_super_user(self):
        self.assertEqual(self.post_resp_super_user.status_code, status.HTTP_201_CREATED)

    def test_view_resolve(self):
        view = resolve(self.url)
        self.assertEqual(view.func.view_class, AttackAPIView)

   

  