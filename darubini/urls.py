from django.urls import path
from .views import (
    AttackAPIView,
    CampaignAPIView,
    TargetWifiAPIView,
    EndCampaignAPIView,
    TargetDeviceAPIView,
    HookedDeviceAPIView,
    AllAPsAPIView,
    FreeWifiAPIView,
    AllCampaignsAPIView,
    UserActivitiesAPIView,
    CampaignTargetsAPIView,
    DeviceProbesAPIView,
    CapturedDevicesAPIView,
    TrafficAPIView,
    LocationFilter,
    FreeWifiTargetDeviceAPIView,
    FreeWifiTrafficAPIView

)

app_name = 'darubini'

urlpatterns = [
    path('attacks', AttackAPIView.as_view(), name='attacks'),
    path('campaigns',  CampaignAPIView.as_view(), name='campaigns'),
    path('campaigns/end/<str:campaign>',
         EndCampaignAPIView.as_view(), name='end_campaign'),
    path('<str:campaign>/target_wifi',
         TargetWifiAPIView.as_view(), name='target_wifi'),
    path('<str:campaign>/<str:wifi>/captured_devices',
         CapturedDevicesAPIView.as_view(), name='captured_devices'),
    path('<str:campaign>/<str:wifi>/traffic',
         TrafficAPIView.as_view(), name='traffic'),
    path('<str:campaign>/<str:wifi>/freewifi_traffic',
         FreeWifiTrafficAPIView.as_view(), name='traffic'),
    path('allaps', AllAPsAPIView.as_view(), name='allaps'),
    path('<str:campaign>/freewifi', FreeWifiAPIView.as_view(), name='freewifi'),
    path('<str:campaign>/<str:wifi>/target_devices',
         TargetDeviceAPIView.as_view(), name="target_device"),
    path('<str:campaign>/<str:wifi>/freewifi_target_devices',
         FreeWifiTargetDeviceAPIView.as_view(), name="target_device"),
    path('hooked_device', HookedDeviceAPIView.as_view(), name='hooked_device'),
    path('all_campaigns', AllCampaignsAPIView.as_view(), name='all_campaigns'),
    path('user_activities', UserActivitiesAPIView.as_view(), name='all_activities'),
    path('campaign_targets', CampaignTargetsAPIView.as_view(),
         name="campaign_Targets"),
    path('<str:campaign>/device_probes',
         DeviceProbesAPIView.as_view(), name="device_probes"),
    path('<str:mac>/locations', LocationFilter.as_view(), name="locations")
]
