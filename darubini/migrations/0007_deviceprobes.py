# Generated by Django 2.2 on 2020-02-16 10:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('darubini', '0006_auto_20200214_1249'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeviceProbes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mac', models.CharField(max_length=17)),
                ('vendor', models.CharField(max_length=100)),
                ('essid', models.CharField(max_length=50)),
                ('campaign', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='device_probes', to='darubini.Campaign')),
            ],
        ),
    ]
