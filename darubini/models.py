from django.db import models


class Campaign(models.Model):
    name = models.CharField(max_length=100, unique=True)
    champion = models.ForeignKey(
        'authentication.User', on_delete=models.DO_NOTHING)
    location = models.CharField(max_length=100, )
    description = models.TextField(blank=True, null=True)
    date = models.DateField(auto_now_add=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class TargetWifi(models.Model):
    name = models.CharField(max_length=100)
    encryption = models.CharField(max_length=10)
    clients = models.IntegerField()
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name="campaign")

    class Meta:
        unique_together = (("name", "campaign"))

    def __str__(self):
        return self.name


class AllAps(models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name="aps")
    name = models.CharField(max_length=100)
    mac = models.CharField(max_length=17)
    vendor = models.CharField(max_length=100, null=True)
    encryption = models.CharField(max_length=10)
    clients = models.IntegerField()

    class Meta:
        unique_together = (('campaign', 'name', 'mac'))

    def __str__(self):
        return self.name


class CapturedDevice(models.Model):
    mac = models.CharField(max_length=17)
    vendor = models.CharField(max_length=100)
    wifi = models.ForeignKey(AllAps, on_delete=models.CASCADE)
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("mac", "wifi"))

    def __str__(self):
        return self.mac


class FreeWifi(models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name='freewifi')
    name = models.CharField(max_length=100)

    class Meta:
        unique_together = (('campaign', 'name'))

    def __str__(self):
        return self.name


class FreeWifiDevice(models.Model):
    name = models.CharField(max_length=100)
    vendor = models.CharField(max_length=100)
    mac = models.CharField(max_length=17)
    ipaddress = models.CharField(max_length=15)
    wifi = models.ForeignKey(
        FreeWifi, related_name='free_devices', on_delete=models.CASCADE)

    class Meta:
        unique_together = (('mac', 'wifi'))

    def __str__(self):
        return self.name

class TargetDevice(models.Model):
    name = models.CharField(max_length=100)
    vendor = models.CharField(max_length=100)
    mac = models.CharField(max_length=17)
    ipaddress = models.CharField(max_length=15)
    wifi = models.ForeignKey(
        TargetWifi, related_name='target_devices', on_delete=models.CASCADE)

    class Meta:
        unique_together = (('mac', 'wifi'))

    def __str__(self):
        return self.name


class FreeWifiTraffic(models.Model):
    # store captured user traffic
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name='freetraffic')
    wifi = models.ForeignKey(
        FreeWifi, on_delete=models.CASCADE, related_name='freetraffic')
    ip_address = models.CharField(max_length=15)
    url = models.CharField(max_length=255)

    class Meta:
        unique_together = (('ip_address', 'wifi', 'url'))

    def __str__(self):
        return self.ip_address


class Traffic(models.Model):
    # store captured user traffic
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name='traffic')
    wifi = models.ForeignKey(
        TargetWifi, on_delete=models.CASCADE, related_name='traffic')
    ip_address = models.CharField(max_length=15)
    url = models.CharField(max_length=255)

    class Meta:
        unique_together = (('ip_address', 'wifi', 'url'))

    def __str__(self):
        return self.ip_address


class HookedDevice(models.Model):
    ip_address = models.GenericIPAddressField()
    os = models.CharField(max_length=20)
    version = models.CharField(max_length=50)
    browser_type = models.CharField(max_length=5)
    session = models.CharField(max_length=255)

    def __str__(self):
        return self.os_version


class Attack(models.Model):
    name = models.CharField(max_length=100)


class Phish(models.Model):
    device = models.OneToOneField(
        'darubini.HookedDevice', on_delete=models.CASCADE)
    attack_type = models.ForeignKey(
        'darubini.Attack', on_delete=models.DO_NOTHING)
    success = models.BooleanField(default=False)


class DeviceProbes(models.Model):
    mac = models.CharField(max_length=17)
    vendor = models.CharField(max_length=100)
    essid = models.CharField(max_length=50)
    campaign = models.ForeignKey(
        Campaign, related_name='device_probes', on_delete=models.CASCADE)

    class Meta:
        unique_together = (('mac', 'vendor', 'essid', 'campaign'))
