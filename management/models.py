from django.db import models

class Profile(models.Model):
    user = models.OneToOneField('authentication.User', on_delete=models.CASCADE)
    bio = models.TextField(blank=False)
    reporting_to = models.ForeignKey('authentication.User', related_name='supervisor', null=True, blank=False, on_delete=models.SET_NULL)
    image = models.ImageField(blank=False) # install pillow
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username


class ProductDetail(models.Model):
    user_doc = models.CharField(max_length=255,blank=True)
    technical_doc = models.CharField(max_length=255, blank=True)
    product_version = models.CharField(max_length=50, default='2.0')
    
    def __str__(self):
        return "Darubini v" + self.product_version
    

class License(models.Model):
    license_types = (
        ('b','basic'),
        ('p', 'premium'),
    )
    client = models.OneToOneField('management.Client', on_delete=models.CASCADE)
    license_type = models.CharField(max_length=1,choices=license_types, default='b')
    start_date = models.DateTimeField(auto_now_add=True)
    end_date = models.DateTimeField()

    def __str__(self):
        return self.client.name
    


class Client(models.Model):
    name = models.CharField(db_index=True, max_length=255, unique=True)
    phone = models.CharField(max_length=100)
    email = models.EmailField()
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
    