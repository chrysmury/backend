from .models import (Profile, ProductDetail, License, Client)
from rest_framework import serializers

class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')
    email = serializers.EmailField(source='user.email')
    bio = serializers.CharField(allow_blank=True, required=False)
    image = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ('username', 'email', 'bio', 'image')
        read_only_fields = ('username', )

    def get_image(self, obj):
        if obj.image:
            return obj.image
        
        return 'http://icons.iconarchive.com/icons/icons8/windows-8/512/Users-Administrator-icon.png'

class ProductDetailSerializer(serializers.ModelSerializer):
    user_doc = serializers.CharField(max_length=255,required=False)
    technical_doc = serializers.CharField(max_length=255,required=False)
    product_version = serializers.CharField(max_length=50, default='2.0')

    class Meta:
        model = ProductDetail
        fields = ('user_doc', 'technical_doc', 'product_version')

    def update(self, instance, validated_data):
        for (key, value) in validated_data.items():
            setattr(instance, key, value)

        return instance
        

    
class LicenseSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='client.name')
    license_type = serializers.CharField(max_length=10)
    start_date = serializers.DateTimeField()
    end_date = serializers.DateTimeField()

    class Meta:
        model = License
        fields = ('name', 'license_type', 'start_date', 'end_date')

    # def update(self, instance, validated_data):
    #     for (key,value) in validated_data.items():
    #         setattr(instance, key, value)
    #     instance.save()
    #     return instance

class ClientRegistrationSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=255)
    phone = serializers.CharField(max_length=100)
    email = serializers.EmailField()

    class Meta:
        model = Client
        fields = ['name', 'phone', 'email', 'created_on']

    def create(self, validated_data):
        return Client.objects.create(**validated_data)


class ClientRetrieveUpdateSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=255)
    phone = serializers.CharField(max_length=100)
    email = serializers.EmailField()

    license_info = LicenseSerializer(write_only=True)
    licence_type = serializers.CharField(source='license_info.license_type', read_only=True)
    start_date = serializers.CharField(source='license_info.start_date', read_only=True)
    end_date = serializers.CharField(source='license_info.end_date', read_only=True)

    class Meta:
        model = Client
        fields = ('name', 'phone', 'email', 'created_on', 'license_info', 'licence_type', 'start_date', 'end_date')
    
    
    def update(self, instance, validated_data):
        license_info = validated_data.pop('license_info', {})
        license_instance = License.objects.get(client=instance)
        for (k, v) in validated_data.items():
            setattr(instance, k, v)

        instance.save()

        if license_info and license_instance:
            for (k, v) in license_info.items():
                setattr(license_instance, k, v)

            license_instance.save()

        return instance



    