from django.apps import AppConfig

class ManagementAppConfig(AppConfig):
    name = 'management'
    label = 'management',
    verbose_name = 'Management'

    def ready(self):
        import management.signals


default_app_config  = 'management.ManagementAppconfig'
