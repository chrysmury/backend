from django.shortcuts import render

from rest_framework import status
from rest_framework.generics import (UpdateAPIView, 
                                        RetrieveAPIView, 
                                        ListAPIView, 
                                        RetrieveUpdateAPIView)
from rest_framework.views import APIView
from rest_framework.permissions import (AllowAny, 
                                        IsAdminUser, 
                                        IsAuthenticated)
from rest_framework.response import Response

from .models import (Profile, 
                        License,
                        ProductDetail,
                        Client)
from .serializers import (ProfileSerializer,
                            ProductDetailSerializer,
                            LicenseSerializer,
                            ClientRegistrationSerializer,
                            ClientRetrieveUpdateSerializer)
from .renderers import (ProfileRenderer,
                        LicenseRenderer,
                        ProductInfoRenderer,
                        ClientRenderer)

from .exceptions import (ProfileDoesNotExist, RecordDoesNotExist, PermissionDenied, ClientIntegrityError, RecordExists)

class ProfileRetrieveAPIView(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ProfileSerializer
    renderer_classes = (ProfileRenderer,)

    def retrieve(self, request, *args, **kwargs):
        # we get the profile of the given username
        # to get any profile
        # profile = Profile.objects.select_related('user').get(user__username=username)
        try:
            profile = request.user.profile
        except Profile.DoesNotExist:
            raise ProfileDoesNotExist

        serializer = self.serializer_class(profile) #remember not to use data=
        return Response(serializer.data, status=status.HTTP_200_OK)


class ProductDetailRetrieveView(ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = ProductDetailSerializer
    renderer_classes = (ProductInfoRenderer, )

    def get(self, request,  *args, **kwargs):
        product_details = ProductDetail.objects.all()
        serializer = self.serializer_class(product_details)
        return Response(serializer.data, status=status.HTTP_200_OK)

class ProductDetailUpdateView(UpdateAPIView):
    permission_classes = (IsAdminUser, )
    serializer_class = ProductDetailSerializer
    renderer_classes = (ProductInfoRenderer, )

    def update(self, request, *args, **kwargs):
        # only superuser can modify this info

        if not request.user.is_superuser:
            raise UpdatePermissionDenied

        # We get the current product info and modify it. We expect only one value will ever be in this table
        instance = ProductDetail.objects.first()

        user_doc = instance.user_doc if hasattr(instance, 'user_doc') else "user_doc"
        technical_doc = instance.technical_doc  if hasattr(instance, 'technical_doc') else "technical_doc"
        version = instance.product_version if hasattr(instance, 'product_version') else "2.0"

        product_details = request.data.get('product_info', {})
        serializer_data = {
            'user_doc':product_details.get('user_doc', user_doc),
            'technical_doc':product_details.get('technical_doc', technical_doc),
            'product_version': product_details.get('product_version', version)
        }
        serializer = self.serializer_class(data=serializer_data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return  Response(serializer.data, status=status.HTTP_200_OK)

class ClientRegistrationAPIView(APIView):
    permission_classes = (IsAdminUser, ) 
    serializer_class = ClientRegistrationSerializer
    renderer_classes = (ClientRenderer, )

    def post(self, request):
        client_data = request.data.get('client', {})
        serializer = self.serializer_class(data=client_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ClientRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    permission_classes = (IsAdminUser, ) 
    serializer_class = ClientRetrieveUpdateSerializer
    renderer_classes = (ClientRenderer, )

    def retrieve(self, request, name, *args, **kwargs):
        serializer_data = Client.objects.get(name=name)
        serializer = self.serializer_class(serializer_data)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    
    def update(self, request, name, *args, **kwargs):
        client_details = request.data.get('client', {})
        instance = Client.objects.get(name=name)
        license_info = client_details['license_info']
        license_instance = License.objects.get(client=instance)
        serializer_data = {
            'name':client_details.get('name', name),
            'phone':client_details.get('phone', instance.phone),
            'email':client_details.get('email', instance.email), 
            'license_instance':license_instance,           
            'license_info':{
                'license_type':license_info['license_type'] if 'license_type' in license_info else license_instance.license_type,
                'start_date': license_info['start_date'] if 'start_date' in license_info else license_instance.start_date,
                'end_date': license_info['end_date'] if 'end_date' in license_info else license_instance.end_date
            }

        }
        serializer = self.serializer_class(instance, data=serializer_data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return  Response(serializer.data, status=status.HTTP_200_OK)

