from django.urls import path

from .views import (
    ProductDetailRetrieveView,
    ProductDetailUpdateView,
    ProfileRetrieveAPIView,
    ClientRetrieveUpdateAPIView,
    ClientRegistrationAPIView
)



app_name = 'management'

urlpatterns = [
    path('profile', ProfileRetrieveAPIView.as_view(), name='profile'),
    path('admin/clients/new', ClientRegistrationAPIView.as_view(), name='clients_new'),
    path('admin/clients/<str:name>', ClientRetrieveUpdateAPIView.as_view(), name='client'),
    path('admin/product_info', ProductDetailUpdateView.as_view(), name='product_info_update'),
    path('product_info', ProductDetailRetrieveView.as_view(), name='product_info'),
]
