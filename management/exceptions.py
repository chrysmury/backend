from rest_framework.exceptions import APIException

# creating custom exceptions in django you import APIExc
# then define the code and detail

class ProfileDoesNotExist(APIException):
    status_code = 404
    default_detail = "The requested profile does not exist"

    
class PermissionDenied(APIException):
    status_4code = 403
    default_detail = "You do not have sufficient privileges"

class ClientIntegrityError(APIException):
    status_code = 400
    default_detail= "Empty Client Details"

class RecordExists(APIException):
    status_code = 400
    default_detail = "This Record already exist"

class RecordDoesNotExist(APIException):
    status_code = 404
    default_detail = "The requested record does not exist"
