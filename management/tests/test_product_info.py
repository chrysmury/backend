from rest_framework.test import APIClient, APITestCase  
from management.views import ProductDetailRetrieveView, ProductDetailUpdateView
from authentication.models import User
from django.urls import resolve, reverse
from rest_framework.test import force_authenticate
from rest_framework import status
from django.test import TestCase
from management.models import ProductDetail


class TestModel(TestCase):
    def setUp(self):
        product = ProductDetail.objects.create(user_doc='user_doc', technical_doc='technical_doc', product_version='2')

    def test_model_creates(self):
        self.assertEqual(ProductDetail.objects.count(), 1)
    
    def test_model_create_correct_record(self):
        self.assertEqual(ProductDetail.objects.first().user_doc, "user_doc")


class TestAuthenticationAccess(APITestCase):

    def setUp(self):
        self.product_info = reverse('management:product_info')

    def test_status_code_no_auth(self):
        resp = self.client.get(self.product_info)
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)


class TestProductInfoAPI(APITestCase):
    def setUp(self):
        self.reg_url = reverse('authentication:register')
        self.product_info = reverse('management:product_info')
        self.product_update = reverse('management:product_info_update')
        self.client = APIClient()

        user_data = {
            'user': {
                'username':'user1',
                'email':'user@email.com',
                'password':'user1@test',
            }
        }
        self.update_info = {
            'product_info':{
                'user_doc':'user_doc'
            }
        }

        self.client.post(self.reg_url, user_data, format='json')

        user = User.objects.get(username='user1')
        self.client.force_authenticate(user)

    def test_auth(self):
        resp = self.client.get(self.product_info)
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        self.assertIn('product_version', resp.data) # default values     

    def test_update_product_info_no_superuser(self):
        resp = self.client.put(self.product_update, self.update_info, format='json')
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_info_superuser(self):
        user = User.objects.create_superuser(username='user2', email='user2@email.com', password='user@test')
        self.client.force_authenticate(user=user)
        resp = self.client.put(self.product_update, self.update_info, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

