from rest_framework.test import APITestCase, APIClient
from management.models import Client, License
from django.urls import reverse, resolve
from rest_framework import status
from authentication.models import User
from management.views import ClientRegistrationAPIView, ClientRetrieveUpdateAPIView

class TestClientLicenseAPI(APITestCase):
    def setUp(self):
        user = User.objects.create_superuser(username='test1', email='test1@test.com', password='test@test')
        self.client_new_url = reverse('management:clients_new')

        self.view_register = resolve(self.client_new_url)

        self.client.force_authenticate(user=user)
        client_data = {
            'client':
            {
                'name':'testclient',
                'phone':'9093493',
                'email':'test@client.io'          
            }
        }
        self.post_resp = self.client.post(self.client_new_url, client_data, format='json')

        self.client_get_url = reverse('management:client', kwargs={'name':'testclient'})
        self.view_update = resolve(self.client_get_url)
        self.get_resp = self.client.get(self.client_get_url)

    def test_resolve_register_view(self):
        self.assertEqual(self.view_register.func.view_class, ClientRegistrationAPIView)

    def test_resolve_update_view(self):
        self.assertEqual(self.view_update.func.view_class, ClientRetrieveUpdateAPIView)

    def test_post_status_code(self):
        self.assertEqual(self.post_resp.status_code, status.HTTP_201_CREATED)
        new_client = Client.objects.last()
        self.assertEqual(new_client.name, 'testclient')
    
    def test_license_creation(self):
        self.assertEqual(License.objects.count(), 1)
        lic = License.objects.last()
        self.assertEqual(lic.client.name, 'testclient')


    def test_post_success(self):
        self.assertEqual(Client.objects.count(), 1)
        self.assertEqual(Client.objects.first().name,'testclient')

    def test_get_client(self):
        self.assertEqual(self.get_resp.status_code, status.HTTP_200_OK)
        self.assertEqual(self.get_resp.data['name'], 'testclient')

    def test_update_client(self):
        update_data = {
            'client':{
                'name':'updatedclient',
                'email':'update@client.io',
                'license_info':{
                    'license_type':'p'
                }
            }
        }
        update_resp = self.client.put(self.client_get_url, update_data, format='json')
        
        self.assertEqual(update_resp.status_code, status.HTTP_200_OK)
        self.assertEqual(update_resp.data['name'], 'updatedclient')

        updated_client = Client.objects.get(name='updatedclient')
        self.assertIsInstance(updated_client, Client) # check if it exists

        client_license = License.objects.get(client=updated_client)
        self.assertEqual(client_license.license_type, 'p')
        

