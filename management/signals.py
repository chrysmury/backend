# we want to create license info when we create a client
from django.db.models.signals import post_save
from django.dispatch import receiver

from management.models import License
from management.models import Client

from django.utils.timezone import timedelta, now

@receiver(post_save, sender=Client)
def create_license_info(sender, instance, created, *args, **kwargs):
    if instance and created:
        instance.license_info = License.objects.create(client=instance, end_date=(now()+timedelta(days=365)))
        print(instance)