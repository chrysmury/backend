from core.renderers import BaseRenderer

class ProfileRenderer(BaseRenderer):
    obj_label = 'profile'

class LicenseRenderer(BaseRenderer):
    obj_label = 'license'

class ProductInfoRenderer(BaseRenderer):
    obj_label = 'product_info'

class ClientRenderer(BaseRenderer):
    obj_label = 'clients'